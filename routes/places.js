var fs = require('fs');
var formidable = require('formidable');
var util = require('util');
var Place = require('../dbmodels/places');

function createPlace(data,user,res){
    console.log(websocket)    //I have the socket so I can emmit something i.e. Notification to users
    var newPlace = new Place({
        'user': user,
        'name': data.name,
        'address': data.address,
        'coordinates': data.coordinates,
        'desc_item':data.desc_item,
        'image_url':(data.image_url)?data.image_url:'noImage'

    });
    newPlace.save(function (e){
        if (e){
            console.log(e)
            return handleError(e);
        }else{
            newPlace.on('es-indexed', function(ee){
                console.log('place was indexed');
                res.send({
                    success: 'here is your acknowledegment for the ready event'
                })
            });
        }
    })
};

exports.newPlace = function(req, res){
    console.log('adding new Place===========================')
    //console.log(req);
    console.log('--------------------------req------')
    console.log(req.body);
    console.log('--------------------------end req------')
    //console.log(req)
    if(req.session.user){
            //check if the app is sending a picture or not
        if(Object.keys(req.body).length==0)
        {
            console.log('image exists')
            console.log(req.body);
            //if image on request then we need to get the image using formidable
            var form = new formidable.IncomingForm(),
                files = [],
                fields={};

            form.uploadDir = './uploadedImages';
            form.on('error', function(err) {
                console.log(err);
                res.send({'error':'error on form upload'});
            });

            form
                .on('field', function(field, value) {
                    //console.log(field, value);
                    //fields.push([field, value]);
                    fields[field] = value;
                })
                .on('file', function(field, file) {
                    //console.log(field, file);
                    files.push([field, file]);
                })
                .on('end', function() {
                    console.log('-> upload done');
                    console.log(files.length)

                    if(files.length!=0)
                    {
                        var tmp_path = files[0][1].path;

                        var public_path = './public';
                        var image_url = '/users/'+req.session.user+'/'+ files[0][1].name;
                        var target_path =public_path+image_url;
                        fs.rename(tmp_path, target_path, function(err) {
                            if(err){
                                console.log('error on rename ')
                                console.log(err); //throw err;
                                // delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files
                            }else
                            {
                                console.log('File has been moved to'+target_path)
                                fields["image_url"] = image_url;
                                createPlace(fields,req.session.user,res)
                                res.send('File uploaded to: ' + target_path);
                            }
                        });
                    }

                });

            form.parse(req);
        }else{
           //else we do not have to save an image
            console.log('no image adding place')
            createPlace(req.body,req.session.user,res);
        }
    }else{
        console.log('No user please login');
        res.send('please login');
    }

};