var User = require('../dbmodels/users');
var fs = require('fs');

exports.login = function(req, res){
    console.log("Starting login process");
    console.log(req.session); //I believe that this it the session cookie that is saved on Redis    s
    if(req.session.user){
        console.log('You have successfully logged in ');
        res.send({res:'success',msg:'you have successfully logged in'})
    }else{
        //Find the user and authenticate him
        // if success then put the user id in the session i.e req.session.user="dasdad"
        // and return success
        // if success then the client should initiate the socket.

        User.findOne({ 'username': req.body.username },function(err,results){
            console.log(results);
            if (err) {
                res.send({error:'success',msg:'Error'});
                console.log(err);
            }
            else if(results)
            {
                if(results.password == req.body.password)
                {
                    //redis_client.setex(rediskey, 86400000, JSON.stringify({id:results._id}), function(err){
                    //    socket.user = results;
                    req.session.user=results._id;
                    console.log('found the user -success.');
                    res.send({res:{
                                    user_id:results._id,
                                    username:results.username,
                                    password:results.password
                                   },
                            msg:'you have successfully logged in'})

                }else
                {
                    console.log('Incorrect password.');
                    res.send({error:'success',msg:'Incorrect password'})
                }
            }
            else{
                console.log('more than one or no user found');
                res.send({error:'success',msg:'more than one user or no user'})
            }
        })
    }


};

exports.register = function(req, res){
    console.log(req.body.username);
    var newuser = new User({
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    });
    newuser.save(function (err) {
        console.log('asdadada');
        if (err){

            res.send({error:'success',msg:'user exists'});
            //return handleError(err);
        }else{
            /*

             This is working and it updates on elastic as well
             newuser.email = 'testme';
             newuser.save(function(er){console.log('2222222222'+er)});

             */
            //need to set the user also
            req.session.user=newuser._id;
            newuser.on('es-indexed', function(ee){
                console.log('newuser was indexed');
                fs.mkdir('./public/users/'+newuser._id,function(e){
                    if(!e || (e && e.code === 'EEXIST')){
                        //do something with contents
                    } else {
                        //debug
                        console.log(e);
                    }
                });
                res.send({res:{
                    user_id:newuser._id,
                        username:newuser.username,
                        password:newuser.password
                },
                    msg:'you have successfully registered'})
            });

        }
    });


};