var fs = require('fs');
var formidable = require('formidable');
var util = require('util');

exports.upload = function(req, res){

    //console.log(req)
    if(req.session.user){

        var form = new formidable.IncomingForm(),
            files = [],
            fields = [];

        form.uploadDir = './uploadedImages';

        form
            .on('field', function(field, value) {
                //console.log(field, value);
                fields.push([field, value]);
            })
            .on('file', function(field, file) {
                //console.log(field, file);
                files.push([field, file]);
            })
            .on('end', function() {
                console.log('-> upload done');
                var tmp_path = files[0][1].path;

                var target_path = './public/users/'+req.session.user+'/'+ files[0][1].name;
                fs.rename(tmp_path, target_path, function(err) {
                    if(err){
                        console.log(err); //throw err;
                        // delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files
                    }else
                    {
                        fs.unlink(tmp_path, function() {
                            if (err){
                                console.log(err);//throw err;
                            }else{
                                res.send('File uploaded to: ' + target_path);
                            }
                        });
                    }
                });



                //console.log(util.inspect(files))

                res.send('received files:\n\n '+util.inspect(files));
            });

        form.parse(req);
    }else{
        console.log('asdadad');
        res.send('please login');
    }

    /*console.log(req.files.image);
     if(req.session.user){
     var tmp_path = req.files.image.path;
     // set where the file should actually exists - in this case it is in the "images" directory
     var target_path = './public/users/'+req.session.user+'/'+ req.files.image.name;
     // move the file from the temporary location to the intended location
     fs.rename(tmp_path, target_path, function(err) {
     if (err) throw err;
     // delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files
     fs.unlink(tmp_path, function() {
     if (err) throw err;
     res.send('File uploaded to: ' + target_path + ' - ' + req.files.image.size + ' bytes');
     });
     });
     }else{
     res.send('please login');
     }*/


};
