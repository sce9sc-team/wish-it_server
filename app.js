
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var files = require('./routes/files');
var places = require('./routes/places');
var http = require('http');
var path = require('path');
var io = require('socket.io');

//setting up Redis and redis store

var connect = require('express/node_modules/connect');
    cookie_ex  = require('express/node_modules/cookie'),
    RedisStore_connect = require('connect-redis')(express),
    RedisStore = require('socket.io/lib/stores/redis'),
    redis  = require('socket.io/node_modules/redis');
    RedisStoreSession = new RedisStore_connect({client: redis.createClient()}),
    redis_pub = redis.createClient(),
    redis_sub =redis.createClient(),
    redis_client =redis.createClient();

//Setting up MongoDB and models
var mongoose = require('mongoose'),
    User = require('./dbmodels/users'),
    Place = require('./dbmodels/places'),
    Notification = require('./dbmodels/notifications'),
    Request = require('./dbmodels/requests');

//var GridStore = mongoose.mongo.GridStore;
//var Grid      = mongoose.mongo.Grid;
//var ObjectID = mongoose.mongo.BSONPure.ObjectID;

mongoose.connect('mongodb://localhost/wishit',function(err) {
    if (err){
        console.log('Could not Connect to Mongo');
        throw err;
    }else{
        console.log('Successfully connected to MongoDB');
    }
});

//========================Requiring socketio routes


var routesio_default =  require('./routesio/default');
var routesio_users = require('./routesio/users');
var routesio_places = require('./routesio/places');
var routesio_notifications = require('./routesio/notifications');
var routesio_requests = require('./routesio/requests');


function initRoutes_default(io_sock,socket){
    console.log('+++++++++++++++++++++++++++');
    var keys = Object.keys(routesio_default);
    for(var d=0;d<keys.length;d++)
    {
        console.log('----------------------initialiszing route : '+ keys[d]);
        routesio_default[keys[d]](io_sock,socket);
    }
}

function initRoutes_users(io_sock,socket){
    console.log('+++++++++++++++++++++++++++');
    var keys = Object.keys(routesio_users);
    for(var d=0;d<keys.length;d++)
    {
        console.log('----------------------initialiszing route : '+ keys[d]);
        routesio_users[keys[d]](io_sock,socket);
    }
}


function initRoutes_places(io_sock,socket){
    console.log('+++++++++++++++++++++++++++');
    var keys = Object.keys(routesio_places);
    for(var d=0;d<keys.length;d++)
    {
        console.log('----------------------initialiszing route : '+ keys[d]);
        routesio_places[keys[d]](io_sock,socket);
    }
}

function initRoutes_notifications(io_sock,socket){
    console.log('+++++++++++++++++++++++++++');
    var keys = Object.keys(routesio_notifications);
    for(var d=0;d<keys.length;d++)
    {
        console.log('----------------------initialiszing route : '+ keys[d]);
        routesio_notifications[keys[d]](io_sock,socket);
    }
}

function initRoutes_requests(io_sock,socket){
    console.log('+++++++++++++++++++++++++++');
    var keys = Object.keys(routesio_requests);
    for(var d=0;d<keys.length;d++)
    {
        console.log('----------------------initialiszing route : '+ keys[d]);
        routesio_requests[keys[d]](io_sock,socket);
    }
}


///------------------------------------------starting express-----
var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
/*
app.use(express.bodyParser({
    uploadDir: __dirname + '/uploadedImages',
    keepExtensions: true
    })
);
*/
app.use(express.json());
app.use(express.urlencoded());
app.use(express.static(path.join(__dirname, 'public')));   //moved it up before cookie so that the images fetched do not create a session to Redis
app.use(express.methodOverride());
app.use(express.cookieParser());
app.use(express.session({
    secret: 'iw2fure,.1',
    store: RedisStoreSession,
    cookie: { secure: false, maxAge:311040000000},
    key:'wishit-sesssion'
}));
app.use(app.router);


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

//Setting Up express paths
app.get('/', routes.index);
app.post('/login',user.login);
app.post('/register',user.register);
app.post('/fileupload',files.upload);
app.post('/newPlace',places.newPlace);

var server = http.createServer(app);
var sio = io.listen(server);

sio.set('store', new RedisStore({
    redisPub: redis_pub,
    redisSub: redis_sub,
    redisClient: redis_client
}));



sio.configure(function (){
    sio.set('authorization', function (data, callback) {
        console.log('authorization started : headers -> '+JSON.stringify(data));
        //console.log('====checking query to get the session=====')
        //console.log(encodeURIComponent(data.query['wishit-sesssion']));
        //console.log('====END checking query to get the session=====')
        if(data.headers.cookie || data.query['wishit-sesssion']) {
            // the data.query['wishit-sesssion'] will be passed by the app since sometimes the app
            //does not pass the cookie
            if(data.query['wishit-sesssion'])
            {
                console.log("cookie in query")
                data.cookie =  cookie_ex.parse(data.query['wishit-sesssion']);
                data.sessionId = connect.utils.parseSignedCookie(data.query['wishit-sesssion'],'iw2fure,.1');
            }
            else{
                console.log("cookie in header")
                data.cookie =  cookie_ex.parse(data.headers.cookie);
                data.sessionId = connect.utils.parseSignedCookie(data.cookie['wishit-sesssion'], 'iw2fure,.1');

            }

            /*
            if(data.headers.cookie){
                console.log("cookie in header")
                data.cookie =  cookie_ex.parse(data.headers.cookie);
                data.sessionId = connect.utils.parseSignedCookie(data.cookie['wishit-sesssion'], 'iw2fure,.1');
            }
            else{
                console.log("cookie in query")
                data.cookie =  cookie_ex.parse(data.query['wishit-sesssion']);
                data.sessionId = connect.utils.parseSignedCookie(data.query['wishit-sesssion'],'iw2fure,.1');
            }    */
            console.log(data.sessionId)
            RedisStoreSession.get(data.sessionId,function(err,session){
                //Get the session from RedisStore
                console.log('trying to find Redis session : '+ err +'---------'+session);
                if(err || !session)
                {
                    console.log('ddddd')
                    callback('Error', false);
                }else
                {
                    //TODO: need to investigate
                    console.log('=============I have a session=======================');
                    console.log(JSON.stringify(session));
                    if(session.user){
                        data.session = session;   //This is available through socket.handshake.session
                        callback(null, true);
                    }else{
                        console.log('aaaaaa')
                        callback('Error', false);
                    }
                }
            });
        }else{
            console.log('No data.headers.cookie');
            callback('Error', false);
        }
    });
});


global.user_sock_redis = redis_client;   // used inorder to keep the users who are online so as to know when to send a socket or push message
global.user_sock={}; //used in order to send messages to individual people
global.websocket;  // used inorder to make available the websocket to the normal post request see places
sio.sockets.on('connection', function (socket) {
    console.log('=============onconnection is fired==============');


    user_sock[socket.handshake.session.user] = socket.id;
    console.log(user_sock);
    // we need to store the userid together with the socket.id of the user so we can send individual messages
    //TODO: do the above in Redis DB for scalling
    //below -save the user id together with the socket id in Redis
    //redis_client.hset("user_sock", socket.handshake.session.user,socket.id);


    socket.join('user:'+socket.handshake.session.user); //Create a room for each user connected
    global.websocket = socket;
    console.log("all rooms");
    console.log(sio.sockets.manager.rooms);

    //Initialize socket methods
    initRoutes_default(sio,socket);
    initRoutes_users(sio,socket);
    initRoutes_places(sio,socket);
    initRoutes_notifications(sio,socket);
    initRoutes_requests(sio,socket);




    //TODO: we need to delete the socket from the global space when user disconnects, best way is to check with the app what is the current socket id and compare it with the previous one
    //TODO: if different send to the server the old for deletion

});

server.listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});

/*

 How to start your DB's
 ---------------------------------------------------

 1) start mongo
 before you need to create the data/db folder inorder to save the db
 got to the folder where mongo is located and start mongo as the user u are with the following option
 /bin/mongod --dbpath the path of the data/db folder - in this project it is located on the root of the directory
 sudo bin/mongod --dbpath ~/work/Node_projects/wishIt1/mongodb/db

 2)start elastic search
 bin/elasticsearch -f

 3)start
 src/redis-server

 how to check your databases
 -----------------------------------------------------
 Redis Commander
 sudo redis-commander   -> install from :  http://nearinfinity.github.io/redis-commander/
 Browser set to ->  http://localhost:8081/

delete redis keys - go to redis src folder and issue
 ./redis-cli KEYS "sess:*" |  xargs ./redis-cli DEL

 elasticsearch plugin -> https://github.com/jettro/elasticsearch-gui
 http://localhost:9200/_plugin/gui/index.html#/query

 elastic search chrome extention  -> Sense
 chrome-extension://doinijnbnggojdlcjifpdckfokbbfpbo/index.html
 aa

 umongo app
 * */