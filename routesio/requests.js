var Request = require('../dbmodels/requests');


function createRequest(sio,socket,from,to,type,res){
    //need to send a notification to the user we are asking to be our friend
    console.log("start creating createRequest");

    var notice = new Request({
        "from": from,
        "to": to,
        "ntype": type,      //friendRequest
        "status":"pending"        //TODO - change the name to "status"
    });
    notice.save(function (err) {
        if (err){
            console.log(err);
            res({
                error: 'Request already exist'
            })
            //return handleError(err);
        }else{
            console.log("Request saved");
            //testing Redis



            // this a test to send notification by getting the socket of a user from Redis
            // PS The below failed and we are using the room from socket.io
            /*
             user_sock_redis.hget("user_sock", to,function (err, replies) {
             console.log("geting the user_sock"+ replies)
             console.log(replies.length )
             sio.sockets.sockets[replies].emit('notification','asdsadada');
             res('notification send to user')
             });
             */

            //send the notification using room
            //TODO: we need to check if user is connect if yes then send him a message if not then send a push notification
            socket.broadcast.to('user:'+to).emit('request', {test:"test"});

            //send notification to the friend's room
            if(user_sock[to]){


                //sio.sockets.sockets[user_sock[to]].emit('notification','asdsadada');
                // res('notification send to user')

            }else{
                res('saved but not Request')
            }
        }
    });
}


exports.sendRequest = function(sio,socket){
    socket.on('sendRequest',function(data,res) {
        console.log("sendRequest started");
        console.log(data);
        //res('notification send to user')
        var userId = socket.handshake.session.user;

        //need to first find if we have a request from the user + and if we have send a request previously
        var qu1 = {"query": {
            "filtered" : {
                "query": {
                    "term" : { "from" :userId}
                },
                "filter" : {
                    "and" : [ {
                        "term" : { "ntype" : "friendrequest"}
                    },{
                        "term" : { "to" : data.to}
                    }]
                }
            }
        }
        };

        var qu = {"query": {
            "filtered" : {
                "query": {
                    "term" : { "from" :data.to}
                },
                "filter" : {
                    "and" : [ {
                        "term" : { "ntype" : "friendrequest"}
                    },{
                        "term" : { "to" : userId}
                    }]
                }
            }
        }
        };


        function searchRequestfromUser(){
            Request.search(qu1, function(err1, result1) {
                console.log("Request search")
                console.log(result1)
                if(err1){
                    console.log("error getting Request from Db")


                }else{
                    if(result1.hits.total != 0)
                    {
                        console.log('you have a pending request')
                        res('you have a pending request')
                    }else{
                        console.log("no Request in Db")
                        createRequest(sio,socket,userId,data.to,data.type,res);
                    }
                }

            });
        }

        Request.search(qu, function(err, result) {
            console.log("Request search")
            console.log(result)
            if(err){
                console.log("error getting Request from Db")
                if(err.toString().indexOf("IndexMissingException")>-1){
                    console.log("first Request on db")
                    createRequest(sio,socket,userId,data.to,data.type,res);
                }
            }else{
                if(result.hits.total != 0)
                {
                    console.log('you have a pending request')
                    res('you have a pending request')
                }else{
                    console.log("no Request in Db")
                    searchRequestfromUser()
                    //createRequest(sio,socket,userId,data.to,data.type,res);
                }
            }

        });





    });
}

exports.readRequests = function(sio,socket){
    socket.on('readRequests',function(data,res) {
        console.log('getting user Requests');
        var userId = socket.handshake.session.user;
        console.log(userId);
        var qu = {"query":
        {
            "multi_match" :
            {
                "query" : userId,
                "fields" : [ "from", "to" ]
            }
        }
        };

        Request.search(qu,{hydrate:true}, function(err, result) {
            if(err){
                res({error:'error'})
            }
            else{

                var requestList = [];
                 // We did the below in order to have the a new list of request with only the relevant part been given to the user
                function createRequestsList(data,i,l){
                    console.log(i + "--"+ l)
                    if(i != l)
                    {
                        if(data[i].from== userId)
                        {
                            data[i].populate('to',function(er,res){
                                var oRequest =  res.toObject();
                                oRequest.user = oRequest.to.username;
                                delete oRequest.from;
                                delete oRequest.to;
                                requestList.push(oRequest);
                                createRequestsList(data,i+1,l);
                            });

                        }
                        else{
                            data[i].populate('from ',function(er,res){
                                var oRequest =  res.toObject();
                                oRequest.user = oRequest.from.username;
                                delete oRequest.from;
                                delete oRequest.to;
                                requestList.push(oRequest);
                                createRequestsList(data,i+1,l);
                            })
                        }
                    }else{
                       console.log(requestList)
                        res(requestList)
                    }
                }

                if(result.hits.length>0){
                    createRequestsList(result.hits,0,result.hits.length);
                }else{ res([])}
            }
        });

        /*
        Request
            .findOne({ from:"52a837fc9ee18ec745000001" })
            .populate('from') // only return the Persons name
            .exec(function (err, re) {
                console.log('asdadad')
                if (err) return handleError(err);
                console.log(re);
                console.log('The creator is %s', re.from);
                // prints "The creator is Aaron"

                console.log('The creators age is %s', re.to);
                // prints "The creators age is null'
            })




        Request.search(qu, function(err, result) {
            // results here

            console.log(result);
            if(err){
                res({error:'error'})
            }
            else{
                if(result.hits.total != 0)
                {
                    res(result.hits.hits)
                }else{
                    res([])
                }
            }
        });
        */

    });
}
