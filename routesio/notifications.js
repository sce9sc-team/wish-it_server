var Notification = require('../dbmodels/notifications');


function createNotification(sio,socket,from,to,type,res){
    //need to send a notification to the user we are asking to be our friend
    console.log("start creating Notification");

    var notice = new Notification({
        "from": from,
        "to": to,
        "ntype": type,      //friendRequest
        "status":"pending"        //TODO - change the name to "status"
    });
    notice.save(function (err) {
        if (err){
            console.log(err);
            res({
                error: 'notification already exist'
            })
            //return handleError(err);
        }else{
            console.log("Notification saved");
            //testing Redis



            // this a test to send notification by getting the socket of a user from Redis
            /*
            user_sock_redis.hget("user_sock", to,function (err, replies) {
                console.log("geting the user_sock"+ replies)
                console.log(replies.length )
                sio.sockets.sockets[replies].emit('notification','asdsadada');
                res('notification send to user')
            });
            */

            //send the notification using room
            socket.broadcast.to('user:'+to).emit('notification', {test:"test"});

            //send notification to the friend's room
            if(user_sock[to]){


                //sio.sockets.sockets[user_sock[to]].emit('notification','asdsadada');
               // res('notification send to user')

            }else{
                res('saved but not notification')
            }
        }
    });
}


exports.sendNotification = function(sio,socket){
    socket.on('sendNotification',function(data,res) {
        console.log("sendNotification started");
        console.log(data);
        /*var userId = socket.handshake.session.user;
         var keys = Object.keys(user_sock);
         for(var i=0;i<keys.length;i++)
         {
         if(keys[i]!=userId){
         var sock_id =user_sock[keys[i]];
         sio.sockets.sockets[sock_id].emit('notification','asdsadada');
         }
         }*/

        //res('notification send to user')
        var userId = socket.handshake.session.user;

        //need to first find if we have a request from the user
        var qu = {"query": {
            "filtered" : {
                "query": {
                    "term" : { "from" :userId}
                },
                "filter" : {
                    "and" : [ {
                        "term" : { "status" : "pending"}
                    }]
                }
            }
        }
        };

        Notification.search(qu, function(err, result) {
            console.log("Notification search")
            console.log(result)
            if(err){
                 console.log("error getting Notification from Db")
                 console.log(err)
                 //TODO -first time there is no data in elastic search
                 //createNotification(sio,socket,userId,data.to,data.type,res);
            }else{
                if(result.hits.total != 0)
                {
                      console.log('you have a pending request')
                      res('you have a pending request')
                }else{
                    console.log("no Notification id Db")
                    createNotification(sio,socket,userId,data.to,data.type,res);
                }
            }

        });





    });
}

exports.getNotifications = function(sio,socket){
    socket.on('getNotifications',function(data,res) {
        console.log('getting user Notifications');
        var userId = socket.handshake.session.user;
        //need to send a notification to the user we are asking to be our friend
        /*var qu1 = {
            "query": {
                "term" : { "to" : userId }   //wildcard
            }
        };*/

        console.log(userId);
        var qu = {"query":
        {
            "multi_match" :
            {
                "query" : userId,
                "fields" : [ "from", "to" ]
            }
        }
        };

        Notification.search(qu, function(err, result) {
            // results here

            console.log(result);
            if(err){
                res({error:'error'})
            }
            else{
                if(result.hits.total != 0)
                {
                    res(result.hits.hits)
                }else{
                    res([])
                }
            }
        });

    });
}
