var _= require("underscore");
var User = require('../dbmodels/users');
var Request = require('../dbmodels/requests');

exports.findFriend =function(sio,socket){
    socket.on('findFriend',function(data,res) {
        console.log(data);
        var userId = socket.handshake.session.user;
        var qu ={"query":
            {
                "filtered" :
                {
                    "query": {
                        "query_string":{"query": data+"*"}
                    },
                    "filter" : {
                        "not" :  {
                            "term" : { "_id" : userId}
                        }
                    }
                }
            }
        };

      /*  var qu1 = {
                    "query": {
                    "query_string":{"query": data+"*"}    //wildcard
                    }
                };*/
        User.search(qu, function(err, result) {
            // results here

            console.log(result);
            if(err){
                res({error:'error'})
            }
            else{
              if(result.hits.total != 0)
              {
                  res(result.hits.hits)
              }else{
                  res([])
              }
            }
        });
    });
}


exports.addFriend = function(sio,socket){
    socket.on('addFriend',function(data,res) {
        console.log("trying to add a friend")
        console.log("requestId = "+data.request_id)
        console.log("user has accepted = "+ data.accept)
        var userId = socket.handshake.session.user;
        //need to find the notification and if accept true then add the friend to both to and from users
        /*var qu1 = {
            "filtered" : {
                "query": {
                    "term" : { "to" : userId }   //wildcard
                },
                "filter" : {
                    "and" : [ {
                        "term" : { "user" : "kimchy"}
                        }

                    ]
                }
            }
        };*/

        var qu ={"query": {
            "ids" : {
                "values" : [data.request_id]
            }
        }}

        console.log("Trying to find the request");
        Request.search(qu,{hydrate:true}, function(err, result) {
            // results here
            console.log("request results")
            console.log(result);
            if(err){
                res({error:'error'})
            }
            else{
                //find the users from and to
                User.findById(result.hits[0].from,function(err,user_from){
                    if(err){
                        res({error:'error',msg:"could not find user from"})
                    }else{
                        console.log(user_from);
                        User.findById(result.hits[0].to,function(err,user_to){
                             if(err){
                                 res({error:'error',msg:"could not find user to"})
                             }else{
                                console.log(user_to);
                                   user_to.friends.push(user_from._id);
                                   user_to.save(function(err){
                                       if(!err){
                                           user_from.friends.push(user_to._id);
                                           user_from.save(function(err){
                                               if(err){
                                                   res({error:'error',msg:"could not save user -from- friend list"})
                                               }else{

                                                   result.hits[0].status = "accept";
                                                   result.hits[0].save(function(err)
                                                   {
                                                       console.log(err)
                                                       if(!err){
                                                           res('success both were saved')

                                                           //TODO: we can send a notification to both users
                                                           console.log(user_to);
                                                           console.log(user_from);
                                                       }else{
                                                           user_to.friends=_.without(user_to.friends, user_from._id);
                                                           user_to.save(function(err){res("reverting")})
                                                       }
                                                   });
                                               }
                                           })
                                       }else{
                                           res({error:'error',msg:"could not save user 'to' friend list"})
                                       }
                                   })
                             }
                        })
                    }
                })
                /*if(result.hits.total != 0)
                {
                    res(result.hits.hits)
                }else{
                    res([])
                }*/
            }
        });
    });
}

/*
'create'
'update'
'delete'
'read'    */

exports.readFriends = function(sio,socket){
    socket.on('readFriends',function(data,res) {
        console.log('readFriends --------')
        console.log(sio.sockets.sockets);
        var userId = socket.handshake.session.user;
        var qu={
            "query": {
                "constant_score" : {
                    "filter" : {
                        "terms" : {
                            "friends" : [userId],
                            "execution" : "bool",
                            "_cache": true
                        }
                    }

                }
            }
        };
        User.search(qu,function(err,friendslist){
            console.log(friendslist);
            if(err){
                res({error:'error'})
            }
            else{
                if(friendslist.hits.total != 0)
                {
                    res(friendslist.hits.hits)
                }else{
                    res([])
                }
            }
        });



    });
};


exports.userStatus = function(sio,socket){
    socket.on('userStatus',function(data,res) {

        console.log("rooms that exist")
        console.log(sio.sockets.manager.rooms);

        //var allrooms = sio.sockets.manager.rooms;
        var userId = socket.handshake.session.user;
        //get user friends
        var qu=
        {"query":
            {
            "term" : { "_id" : userId }
            }
        }
        User.search(qu,function(err,user){
            console.log("usersStatus ---------- get Friends")
            console.log(user);
            if(err){
                res({error:'error'})
            }
            else{
                if(user.hits.total != 0)
                {
                    var friendsList = user.hits.hits[0]._source.friends
                    console.log(friendsList)
                    for(var i=0;i<friendsList.length;i++)
                    {
                       // console.log("-----------------Friends---------")
                       // console.log(allrooms['/user:'+friendsList[i]]) ;
                       // if (allrooms['/user:'+friendsList[i]])
                        //{
                         //   console.log("friend that has room :"+friendsList[i]);
                            socket.join('user:'+friendsList[i]);
                            socket.broadcast.to('user:'+friendsList[i]).emit("userStatus",{status:"online",user_id:userId});
                       // }
                    }
                    res('done');
                }else{
                    res('error');
                }
            }
        });

    });
}

exports.userStatusReply = function(sio,socket){
    socket.on('userStatusReply',function(data,res) {
        var userId = socket.handshake.session.user;
        socket.broadcast.to('user:'+data.friend_id).emit("userStatusReply",{status:"online",user_id:userId});
    });
}