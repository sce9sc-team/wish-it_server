var Place = require('../dbmodels/places');
var Users = require('../dbmodels/users');
var fs = require('fs');

exports.readPlaces = function(sio,socket){
    socket.on('readPlaces',function(data,res) {
        console.log("trying to get the places");
        var userId = socket.handshake.session.user;
                Place.search({query:userId}, function(err, results) {
                    // results here
                    console.log(res);
                    res(results.hits.hits);
                });
    });
};

exports.deletePlace = function(sio,socket){
    socket.on('deletePlace',function(data,res) {
        //It also deletes the index from elastic search
        Place.findById(data.id, function ( err, place ){
          place.remove( function (er, d ){
                console.log(er)
                if(er){
                    console.log("an error occurred :"+er) ;
                }else{
                    console.log(place.image_url);
                        if(place.image_url !='noImage'){
                            fs.unlink('./public'+place.image_url, function (err) {
                                if (err) throw err;
                                console.log('successfully deleted the place image');
                                res('success in deleting the place');
                            });
                        }else{
                            res('success in deleting the place');
                        }
                }
            });
        });
    });
};


exports.getAllFriendsPlaces =  function(sio,socket){
    socket.on('getAllFriendsPlaces',function(data,res) {
        var userId = socket.handshake.session.user;
        console.log(userId);
        var quUser = {
            "query": {
                "term" : { "_id" : userId }
            }
        }

        console.log("starting getALlFriendsPlaces ")
        //need to find the users friends first
        Users.search(quUser, function(err, results) {
            console.log("result of gettting the user")
            console.log(results);
            if(err)
            {
                console.log(err)
                res({error:err})
            }
            else
            {
                console.log("searching for Places")
                console.log(results.hits.hits)
                var listOfFriends = results.hits.hits[0]._source.friends;
                console.log(listOfFriends);
                //res(results.hits.hits[0].friends);
                var qu = {
                    "query":
                        {
                            "terms" : {
                                "user" : listOfFriends
                            }
                        }
                }

                Place.search(qu, function(err1, results1) {
                    // results here
                    console.log(results1);
                    if(err1)
                    {
                        console.log(err1)
                        res({error:err1})
                    }
                    else{
                        res(results1.hits.hits);
                    }
                });
            }
        });
    })
}

exports.getFriendsPlaces = function(sio,socket){
    socket.on('getFriendsPlaces',function(data,res) {
        console.log("trying to get the getFriendsPlaces")


        var userId = socket.handshake.session.user;
        console.log(userId);
        var quUser = {
            "query": {
                "term" : { "_id" : userId }
            }
        }

        console.log("starting getALlFriendsPlaces ")
        //need to find the users friends first
        Users.search(quUser, function(err, results) {
            console.log("result of gettting the user")
            console.log(results);
            if(err)
            {
                console.log(err)
                res({error:err})
            }
            else
            {
                console.log("searching for Places")
                console.log(results.hits.hits)
                var listOfFriends = results.hits.hits[0]._source.friends;
                listOfFriends.push(userId)//add also yours
                console.log(listOfFriends);
                // This is for multiple users
                var qu = {
                    "query":{
                        "filtered" : {
                            "query" : {
                                "terms" : { "user" : listOfFriends }
                            },
                            "filter" : {
                                "geo_distance" : {
                                    "distance" : "2km",
                                    "coordinates" : data.coordinates
                                }
                            }
                        }
                    }
                }

                //This is for one user change the above to the below
                //"term" : { "user" : "52875f63c7b0d79004000001" }

                Place.search(qu, function(err1, results1) {
                    // results here
                    console.log(results1);
                    if(err1)
                    {
                        console.log(err1)
                        res({error:err1})
                    }
                    else{
                        console.log(results1.hits.hits)
                        res(results1.hits.hits);
                    }
                });
            }
        });
    });
};




/*{query:{
 "filtered" : {
 "query" : {
 "match_all" : {}
 },
 "filter" : {
 "geo_distance" : {
 "distance" : "200km",
 "pin.location" : {
 "lat" : 40,
 "lon" : -70
 }
 }
 }
 }
 }
 }*/


/* -----------This is done by an http request not socket anymore  --------    -
exports.addPlace =  function(sio,socket){
    socket.on('addPlace',function(data,res) {
        console.log('trying to add a place ')
        console.log(this)
        if(this.user)
        {
            var newPlace = new Place({
                'name': 'testPlace1',
                'address': 'I.Polemi 42',
                'coordinates': '38.049173,23.806633',
                'user': this.user
            });
            newPlace.save(function (e){
                if(e){
                    console.log(e)
                    return handleError(e);
                }else{
                    newPlace.on('es-indexed', function(ee){
                        console.log('place was indexed');
                    });
                    res({
                        success: 'here is your acknowledegment for the ready event'
                    })
                }
            })
        }
    });



    /*var newPlace = new Place({
     'name': 'testPlace1',
     'address': 'I.Polemi 42',
     'coordinates': '[1231313,132123]'
     });
     newPlace.save(function (err){

     var newuser = new User({ email: 'sce9sc_2@hotmail.com',
     username: "sce9sc_2",
     password:"iw2fure,.1",
     places:newPlace});

     newuser.save(function (err) {
     if (err){
     console.log(err)
     return handleError(err);
     }else{
     req.io.respond({
     success: 'here is your acknowledegment for the ready event'
     })
     }
     })


     });
     */

//}

//-----------This is done by an http request not socket anymore  --------    -