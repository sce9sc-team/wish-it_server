

window.onload = function(){

    login();

}


function login(){
    var user =$('#l_username').val();
    var pass =$('#l_password').val();
    $.ajax({
        url: '/login',
        type: 'POST',
        contentType: 'application/json',
        dataType:'json',
        data: JSON.stringify({'username':user,'password':pass}),
        success:startSocketio,
        error:errorfn
    });
}


function register(){
    var user =$('#r_username').val();
    var pass =$('#r_password').val();
    var email =$('#r_email').val();

    $.ajax({
        url: '/register',
        type: 'POST',
        contentType: 'application/json',
        dataType:'json',
        data: JSON.stringify({'username':user,'password':pass,'email':email}),
        success:successRegister,
        error:errorfn
    });

}

function errorfn(xhr, errorType, error)
{
    console.log(errorType)
}

function startSocketio(data, status, xhr){
    console.log(data);
    if(!data.error){
        window.sio = io.connect('http://localhost');
        sio.on('connect', function () {
            alert('you are connected')
        });
        sio.on('error', function (reason){
            console.log('Unable to connect Socket.IO', reason);
        });

        sio.on('notification',function(data){
            alert(data)
        })
    }
}

function successRegister(data, status, xhr){
  console.log(data);
}


function uploadFile(e){
    var file =$('#file1').val();
    $.ajax({
        url: '/fileupload',
        type: 'POST',
        enctype:"multipart/form-data",
        contentType: 'multipart/form-data',
        data: file,
        success:successFileUpload,
        error:errorfn
    });
}


function successFileUpload(data, status, xhr){
    console.log(data);
}
