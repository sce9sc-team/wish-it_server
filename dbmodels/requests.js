var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    mongoosastic = require('mongoosastic');


var RequestSchema = new Schema({
    'from': { type: Schema.ObjectId, ref: 'User', index: true,required: true  },
    'to': { type: Schema.ObjectId, ref: 'User', index: true,required: true  },
    'ntype': { type: String, required: true },
    'status':  { type: String, required: true },
    'created': { type: Date, default: Date.now , required: true }
});

RequestSchema.plugin(mongoosastic);

module.exports = mongoose.model('Request', RequestSchema);
