var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    mongoosastic = require('mongoosastic');


var UserSchema = new Schema({
    'email': { type: String, index: { unique: true } },
    'username': { type: String, required: true, index: { unique: true } },
    'password': { type: String, required: true },
    'friends': [{ type: Schema.ObjectId, ref: 'User', index: true }],
    'phoneNumber':{ type: Number, max: 10 },
    'created': { type: Date, default: Date.now , required: true }
    //'places':{ type: Schema.ObjectId, ref: 'Place', index: true, es_indexed:true }
});

UserSchema.plugin(mongoosastic);

module.exports = mongoose.model('User', UserSchema);
