var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    mongoosastic = require('mongoosastic');



var PlacesSchema = new Schema({
    'user':{ type: Schema.ObjectId, ref: 'User', index: true,es_type:"string" },
    'name': { type: String,es_type:"string"},
    'address': { type: String, required: true, index: true, es_type:"string"  },
    'coordinates': { type: String, required: true ,es_type:"geo_point"},
    'desc_item':{ type: String,es_type:"string"},
    'image_url':{ type: String,es_type:"string"},
    'created': { type: Date, default: Date.now , required: true,es_type:"date" }
});

PlacesSchema.plugin(mongoosastic);

var Places = mongoose.model('Place', PlacesSchema);
Places.createMapping(function(err, mapping){
    // do neat things here
    console.log('createMap Place');
    console.log(err);
});

module.exports = Places;
