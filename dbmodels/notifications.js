var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    mongoosastic = require('mongoosastic');


var NotificationSchema = new Schema({
    'from': { type: Schema.ObjectId, ref: 'User', index: true,required: true  },
    'to': { type: Schema.ObjectId, ref: 'User', index: true,required: true  },
    'ntype': { type: String, required: true },
    'status':  { type: String, required: true },   //TODO - change the name to "status"
    'created': { type: Date, default: Date.now , required: true },
});

NotificationSchema.plugin(mongoosastic);

module.exports = mongoose.model('Notification', NotificationSchema);
